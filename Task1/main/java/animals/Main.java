package animals;

public class Main {
    public static void main(String[] args) {
        Bird myBird = new Bird("Kesha");
        myBird.eat();
        myBird.fly();
        myBird.sleep();

        Fish myFish = new Fish("Nemo");
        myFish.eat();
        myFish.swim();
        myFish.swim();

        Eagle myEagle = new Eagle("Joey");
        myEagle.eat();
        myEagle.fly();
        myEagle.sleep();
    }
}
