package animals;

public class Eagle extends Bird {
    public Eagle(String name) {
        super(name);
    }

    @Override
    protected void eat() {
        System.out.println(String.format("Eagle %s eats meet", this.name));
    }

    @Override
    protected void sleep() {
        System.out.println(String.format("Eagle %s is asleep", this.name));
    }

    @Override
    protected void fly() {
        System.out.println(String.format("Eagle %s flies", this.name));
    }
}
