package animals;

public class Fish extends Animal {
    public Fish(String name) {
        this.name = name;
    }

    @Override
    public void sleep() {
        System.out.println(String.format("Fish %s is asleep", this.name));
    }
    @Override
    public void eat() {
        System.out.println(String.format("Fish %s eats anything", this.name));
    }

    public void swim(){
        System.out.println(String.format("Fish %s swims", this.name));
    }
}
