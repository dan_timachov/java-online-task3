package animals;

public class Parrot extends Bird {
    public Parrot(String name) {
        super(name);
    }

    @Override
    protected void eat() {
        System.out.println(String.format("Parrot %s eats plants"));
    }

    @Override
    protected void sleep() {
        System.out.println(String.format("Parrot %s is asleep", this.name));
    }

    @Override
    protected void fly() {
        System.out.println(String.format("Parrot %s flies", this.name));
    }
}
