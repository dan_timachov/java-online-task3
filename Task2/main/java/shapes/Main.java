package shapes;

public class Main {
    public static void main(String[] args) {
        Rectangle redRect = new Rectangle("Red", 10, 15);
        Rectangle yellowRect = new Rectangle("Yellow", 4, 11);
        Rectangle blueRect = new Rectangle("Blue", 2, 10);

        Triangle orangeTria = new Triangle("Orange");
        orangeTria.setX(new int[]{1, 2, 3});
        orangeTria.setY(new int[]{3, 4, 5});
        System.out.println(orangeTria.getPerimeter());

        Circle greenCircle = new Circle("Green", 10);
        System.out.println(greenCircle.getArea());

    }
}
