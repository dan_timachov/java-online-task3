package shapes;

public class Triangle extends Shape {
    private int[] x = new int[3];
    private int[] y = new int[3];

    Triangle(String color) {
        super(color);
    }


    public int[] getX() {
        return x;
    }

    public void setX(int[] x) {
        this.x = x;
    }

    public int[] getY() {
        return y;
    }

    public void setY(int[] y) {
        this.y = y;
    }

    private double getLength(int x1, int y1, int x2, int y2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    public  double getPerimeter(){
        double a = getLength(x[0], y[0], x[1], y[1]);
        double b = getLength(x[0], y[0], x[2], y[2]);
        double c = getLength(x[2], y[2], x[1], y[1]);

        return a + b + c;
    }
}
